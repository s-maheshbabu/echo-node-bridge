// used http://jsbeautifier.org/ to format the script below
// XML is attached
 
 
 
var fs = require('fs');
var path = require('path');
var dgram = require('dgram');
var http = require('http');
 
var client = dgram.createSocket('udp4');
var PORT = 1900;
var HOST = '10.0.0.200';//'10.0.0.46'; // This is your local IP
var UPNP_RESPONSE_PORT = '50000';
var SSDP_ADDRESS = '239.255.255.250',
    SSDP_PORT = 1900;
var myUUID = "88f6698f-2c83-4393-bd03-cd54a9f8595"; // random UUID
var PACKET_TYPE = 'MAN',
    SEARCH_TARGET = 'ST',
    WAIT_TIME = 'MX',
    LOCATION = 'LOCATION'; // some ssdp terminology
var filePath = path.join(__dirname, 'setup.xml');
var CONTROLHOSTHOST = "localhost:8080/pij/ControlPanelServlet";
 
/*** Mocked see end of script for json light objects
* General:
* {"lights":{"4321":{"state":{"on":false,"bri":254,"hue":15823,"sat":88,"effect":"none","ct":313,"alert":"none","colormode":"ct","reachable":true,"xy":[0.4255,0.3998]},"type":"Extended color light","name":"Green Light","modelid":"LCT001","manufacturername":"Philips","uniqueid":"4321","swversion":"65003148","pointsymbol":{"1":"none","2":"none","3":"none","4":"none","5":"none","6":"none","7":"none","8":"none"}},"8765":{"state":{"on":false,"bri":254,"hue":15823,"sat":88,"effect":"none","ct":313,"alert":"none","colormode":"ct","reachable":true,"xy":[0.4255,0.3998]},"type":"Extended color light","name":"Blue Light","modelid":"LCT001","manufacturername":"Philips","uniqueid":"8765","swversion":"65003148","pointsymbol":{"1":"none","2":"none","3":"none","4":"none","5":"none","6":"none","7":"none","8":"none"}},"1019":{"state":{"on":false,"bri":254,"hue":15823,"sat":88,"effect":"none","ct":313,"alert":"none","colormode":"ct","reachable":true,"xy":[0.4255,0.3998]},"type":"Extended color light","name":"Red Light","modelid":"LCT001","manufacturername":"Philips","uniqueid":"1019","swversion":"65003148","pointsymbol":{"1":"none","2":"none","3":"none","4":"none","5":"none","6":"none","7":"none","8":"none"}}}}
*
 * Lights only:
* {"4321":"Green Light","8765":"Blue Light","1019":"Red Light"}
*
 *
 */
 
var server = http.createServer(function(req, res) {
        var urlArray = ((req.url).split("/"));
        console.log(urlArray);
        console.log("--------------=============");
        console.log(req.body);
        console.log("--------------=============");
        var urlArrayLen = urlArray.length;
 
        if (urlArrayLen == 2 && urlArray[1] == 'favicon.ico') {
            res.end();
        }
 
        if (urlArrayLen == 2 && urlArray[1] == 'setup.xml') {
            res.writeHead(200, {
                'Content-Type': 'text/xml'
            });
            //res.write('request successfully proxied to port 9000!' + '\n' + JSON.stringify(req.headers, true, 2));
            myXML = fs.readFileSync(filePath);
            res.write(myXML);
            res.end();
            console.log("Someone queired me for configuration!");
        } 
	else if (urlArrayLen > 2 && urlArray[urlArrayLen - 1] == 'SJJbMh1ATKIcDrSrSeYw1TCDThpHfMnqfpdkMzfN') {
            console.log(req.url, "Alexa queries me for all!");
            res.writeHead(200, {
                'Content-Type': 'application/json',
                'charset': 'UTF-8'
            });
            console.log(JSON.stringify(ALL_INFO));
            res.write(JSON.stringify(ALL_INFO));
        } 
	else if (urlArrayLen > 2 && urlArray[urlArrayLen - 1] == 'lights') {
            console.log("Alexa queries me for lights! ");
            res.writeHead(200, {
                'Content-Type': 'application/json'
            });
            res.write(JSON.stringify(LIGHTS));
        } else if (urlArrayLen > 2 && urlArray[urlArrayLen - 1] == 'state') {
            console.log("Alexa queries me for " + urlArray[urlArrayLen - 2] + " light!");
            res.writeHead(200, {
                'Content-Type': 'application/json'
            });
            fakestate = !fakestate;
            res.write('{"4321":"Office Light","8765":"Hutch Light","1019":"Study Light"}');
 
            var pin = "09";
 
            if (urlArray[urlArrayLen - 2] == "4321") {
                pin = "09";
            } else if (urlArray[urlArrayLen - 2] == "8765") {
                pin = "10";
            } else {
                pin = "11";
            }
 
            http.get("http://"+CONTROLHOSTHOST+"?applnc=bed3&state="+(fakestate==true?"1":"0"), function(res) {
		console.log("http://"+CONTROLHOSTHOST+"?applnc=bed3&state="+(fakestate==true?"1":"0"));
                console.log("Got response: " + res.statusCode);
            }).on('error', function(e) {
                console.log("Got error: " + e.message);
            });
 
 
        } else {
 
            res.writeHead(200, {
                'Content-Type': 'text/html'
            });
            res.write(JSON.stringify(STATE));
 
        }
 
        res.end();
    });
 
server.listen(9000, HOST);
/*
var server = http.createServer(function(request, response) {
var filePath = path.join(__dirname, 'setp.xml');
var stat = fs.statSync(filePath);
response.writeHead(200, {
'Content-Type': 'text/xml',
'Content-Length': stat.size
});
 
//var readStream = fs.createReadStream(filePath, 'ascii');
//readStream.pipe(response);
response.
response.end();
});
server.listen(8080, HOST);
*/
 
var discoveryMessage = ("HTTP/1.1 200 OK\r\n" + "CACHE-CONTROL: max-age=86400\r\n" + "EXT:\r\n" + "LOCATION: http://"+HOST+":9000/setup.xml\r\n" + "OPT: \"http://schemas.upnp.org/upnp/1/0/\"; ns=01\r\n" + "01-NLS: "+HOST+"\r\n" + "ST: urn:schemas-upnp-org:device:basic:1\r\n" + "USN: uuid:Socket-1_0-221438K0100073::urn:Belkin:device:\*\*\r\n\r\n");
 
client.on('listening', function() {
    var address = client.address();
    //console.log('UDP Client listening on ' + address.address + ":" + address.port);
   client.setMulticastTTL(128);
   client.addMembership('239.255.255.250', HOST);
});
 
client.on('message', function(msg, remote) {
    var headers = (parseMsearchRequest(msg))
    if (headers === null)
        return;
    console.log(headers);
    if (headers[PACKET_TYPE] == '"ssdp:discover"') {
        console.log('UPnP Broadcast recieved.');
        console.log('From: ' + remote.address + ':' + remote.port + ' \n\n' + msg + "--------");
        var responseSocket = dgram.createSocket('udp4');
 
        var message = new Buffer(discoveryMessage);
 
        responseSocket.send(message, 0, message.length, remote.port,
            remote.address,
            function(err, bytes) {
                if (err)
                //console.log("========\n" + err + "\n========");
                    if (bytes)
                    //console.log("--------\n" + message + "\n--------");
                        responseSocket.close();
            });
 
    }
 
});
 
client.bind(PORT);
 
function parseLines(lines) {
    var headers = {};
    for (var i = 1; i < lines.length; i++) {
        var colonPos = lines[i].indexOf(':');
        if (colonPos === -1)
            continue;
        headers[lines[i].substr(0, colonPos).toUpperCase()] = lines[i].substr(
            colonPos + 1).trim();
    }
    return headers;
}
 
function parseMsearchRequest(msg) {
    var lines = msg.toString().split('\r\n');
    if (!/^M-SEARCH \* HTTP\/1.1/.test(lines[0]))
        return false;
    return parseLines(lines);
}
 
function parseMsearchResponse(msg) {
    var lines = msg.toString().split('\r\n');
    if (!/^HTTP\/1.1 200 OK/.test(lines[0]))
        return null;
    return parseLines(lines);
}
 
var ALL_INFO = {
    "lights": {
        "4321": {
            "state": {
                "on": false,
                "bri": 254,
                "hue": 15823,
                "sat": 88,
                "effect": "none",
                "ct": 313,
                "alert": "none",
                "colormode": "ct",
                "reachable": true,
                "xy": [0.4255, 0.3998]
            },
            "type": "Extended color light",
            "name": "Office Light",
            "modelid": "LCT001",
            "manufacturername": "Philips",
            "uniqueid": "4321",
            "swversion": "65003148",
            "pointsymbol": {
                "1": "none",
                "2": "none",
                "3": "none",
                "4": "none",
                "5": "none",
                "6": "none",
                "7": "none",
                "8": "none"
            }
        },
        "8765": {
            "state": {
                "on": false,
                "bri": 254,
                "hue": 15823,
                "sat": 88,
                "effect": "none",
                "ct": 313,
                "alert": "none",
                "colormode": "ct",
                "reachable": true,
                "xy": [0.4255, 0.3998]
            },
            "type": "Extended color light",
            "name": "Hutch Light",
            "modelid": "LCT001",
            "manufacturername": "Philips",
            "uniqueid": "8765",
            "swversion": "65003148",
            "pointsymbol": {
                "1": "none",
                "2": "none",
                "3": "none",
                "4": "none",
                "5": "none",
                "6": "none",
                "7": "none",
                "8": "none"
            }
        },
        "1019": {
            "state": {
                "on": false,
                "bri": 254,
                "hue": 15823,
                "sat": 88,
                "effect": "none",
                "ct": 313,
                "alert": "none",
                "colormode": "ct",
                "reachable": true,
                "xy": [0.4255, 0.3998]
            },
            "type": "Extended color light",
            "name": "Study Light",
            "modelid": "LCT001",
            "manufacturername": "Philips",
            "uniqueid": "1019",
            "swversion": "65003148",
            "pointsymbol": {
                "1": "none",
                "2": "none",
                "3": "none",
                "4": "none",
                "5": "none",
                "6": "none",
                "7": "none",
                "8": "none"
            }
        }
    }
};
 
var LIGHTS = {
    "4321": "Office Light",
    "8765": "Hutch Light",
    "1019": "Study Light"
}
 
var fakestate = true;
var STATE = {
    "state": {
        "on": fakestate,
        "bri": 254,
        "hue": 15823,
        "sat": 88,
        "effect": "none",
        "ct": 313,
        "alert": "none",
        "colormode": "ct",
        "reachable": true,
        "xy": [0.4255, 0.3998]
    },
    "type": "Extended color light",
    "name": "Red Light",
    "modelid": "LCT001",
    "manufacturername": "Philips",
    "uniqueid": "1019",
    "swversion": "65003148",
    "pointsymbol": {
        "1": "none",
        "2": "none",
        "3": "none",
        "4": "none",
        "5": "none",
        "6": "none",
        "7": "none",
        "8": "none"
    }
};
