process.env['NODE_CONFIG_DIR']='/home/pi/Development/EchoNodeBridge/config'

var fs = require('fs');
var path = require('path');
var dgram = require('dgram');
var http = require('http');                                                     // remove ... this is refactored to tomcat
var config = require('config');
var ip = require('ip');


/*
 * Working with Node.js as webserver
 * "LOCATION: http://" + ip.address() +":" + config.get("controller.port") + "/" + config.get("controller.path") + "\r\n" + 
 * Not working with Tomcat Serving the files
 * "LOCATION: http://" + ip.address() +":8080/smarthome/setup.xml\r\n" +
        
 */

// some ssdp terminology
PACKET_TYPE = 'MAN',
SEARCH_TARGET = 'ST',
WAIT_TIME = 'MX',
LOCATION = 'LOCATION'; 
DISCOVERY_MESSAGE = "HTTP/1.1 200 OK\r\n" + 
        "CACHE-CONTROL: max-age=86400\r\n" + 
        "EXT:\r\n" + 
        "LOCATION: http://" + ip.address() +":" + config.get("controller.port") + "/" + config.get("controller.path") + "\r\n" + 
        "OPT: \"http://schemas.upnp.org/upnp/1/0/\"; ns=01\r\n" + 
        "01-NLS: "+ ip.address() +"\r\n" + 
        "ST: urn:schemas-upnp-org:device:basic:1\r\n" + 
        "USN: uuid:Socket-1_0-221438K0100073::urn:Belkin:device:\*\*\r\n\r\n";
 

// begin UPNP Discovery Listener Implementation
var client = dgram.createSocket('udp4');
client.on('listening', function () {
    var address = client.address();
    console.log('UDP Client listening on ' + address.address + ":" + address.port);
                                                                                // debugging

    client.setMulticastTTL(128);
    client.addMembership(config.get("client.ssdpAddress"), ip.address());
});


client.on('message', function (msg, remote) {
    var headers = (parseMsearchRequest(msg))
    if (headers === null){
        return;
    }
    if (headers[PACKET_TYPE] == '"ssdp:discover"') {
        console.log('UPnP Broadcast recieved.');
        console.log('From: ' + remote.address + ':' + remote.port + ' \n\n' + msg + "--------");
        var responseSocket = dgram.createSocket('udp4');

        var message = new Buffer(DISCOVERY_MESSAGE);
        responseSocket.send(message, 0, message.length, remote.port,remote.address,
                function (err, bytes) {
                    if (err) {
                        console.log("========\n" + err + "\n========");
                        if (bytes) {
                            //console.log("--------\n" + message + "\n--------");
                            responseSocket.close();
                        }
                    }
                });

    }

});
client.bind(config.get("client.ssdpPort"), function () {
    console.log("bound")
});


/** ************************************************ **/
var filePath = path.join(__dirname, config.get("controller.path"));
var server = http.createServer(function (req, res) {
    if (req.url == "/favicon") res.end();
    
    
    console.log(req.url);
    if (req.url == "/config/setup.xml") {
        res.writeHead(200, {
            'Content-Type': 'text/xml'
        });

        myXML = fs.readFileSync(filePath);
        res.write(myXML);
        res.end();
    } else {
        //console.log((req.url).match(/^(\/api)+(\/[a-zA-Z0-9]+)$/));
        //************************************************************
        if ((req.url).match(/^(\/api)+(\/[a-zA-Z0-9]+)$/)) {
            console.log("api and device id");
            // if we are here, return a consolidated inventory of all lights.


            res.writeHead(200, {'Content-Type': 'application/json'});

            var statePath = path.join(__dirname, config.get("bookmarks.states"));
            var state = JSON.parse(fs.readFileSync(statePath));
            var lights = {};

            for (i in state.lights) {
                state.states["name"] = state.lights[i];

                states = {
                    "state": {
                        "on": false,
                        "bri": 254,
                        "hue": 15823,
                        "sat": 88,
                        "effect": "none",
                        "ct": 313,
                        "alert": "none",
                        "colormode": "ct",
                        "reachable": true,
                        "xy": [0.4255, 0.3998]
                    },
                    "type": "Extended color light",
                    "name": state.lights[i],
                    "modelid": "LCT001",
                    "manufacturername": "Philips",
                    "uniqueid": i,
                    "swversion": "65003148",
                    "pointsymbol": {
                        "1": "none",
                        "2": "none",
                        "3": "none",
                        "4": "none",
                        "5": "none",
                        "6": "none",
                        "7": "none",
                        "8": "none"
                    }
                }
                lights[i] = states;

                fs.writeFile("./config/" + i + ".json", JSON.stringify(lights[i]));
            }

            var allInfo = '{"lights":' + JSON.stringify(lights) + "}";
            console.log(allInfo);

            res.write(allInfo);
            res.end();

            fs.writeFile("./config/all.json", allInfo);
        } else if ((req.url).match(/^\/api\/[a-zA-Z0-9]+(\/lights)$/)) {
            console.log("query for lights -- returning json for lights"); //
            // if we are here, return lights json
            res.writeHead(200, {'Content-Type': 'application/json'});
            var lightPath = path.join(__dirname, config.get("bookmarks.states"));

            var lights = JSON.parse(fs.readFileSync(lightPath));
            console.log(JSON.stringify(lights.lights));
            res.write(JSON.stringify(lights.lights));
            res.end();
        } else if ((req.url).match(/^\/api\/[a-zA-Z0-9]+(\/lights)\/[a-zA-Z0-9]+(\/state)$/)) {
            console.log("check and writing state to the file. send command to swtich lights on or off")
            var statePath = path.join(__dirname, config.get("bookmarks.states"));
            var state = JSON.parse(fs.readFileSync(statePath));
            var lights = {};
            //var controlURL = "http://"+ip.address()+":8080/"
	    var controlURL = config.get("controller.fullAddress")
            for (i in state.lights) {    
                if ((req.url).indexOf(i)>0) {
                    console.log((req.url).indexOf(i));
                    var swtichId = (req.url).substring((req.url).indexOf(i), (req.url).indexOf("/state"));
                    
                    var states = JSON.parse(fs.readFileSync("config/" + swtichId + ".json"));
                    states.state.on = !states.state.on;
                    fs.writeFile("./config/" + i + ".json", JSON.stringify(states));
                    
                    //controlURL += (config.get("switchConfig."+swtichId+".partURL")+config.get("switchConfig."+swtichId+"."+(states.state.on?"on":"off")));
                    controlURL += (config.get("switchConfig."+swtichId+".partURL"));
                    console.log(controlURL);
                    break;
                }
            }
            res.writeHead(200, {
                'Content-Type': 'application/json'
            });
            var lightPath = path.join(__dirname, config.get("bookmarks.states"));
            var lights = JSON.parse(fs.readFileSync(lightPath));
            res.write(JSON.stringify(lights.lights));
            res.end();
            
            
            http.get(controlURL, function(res) {
		console.log("request to: " + controlURL);
                console.log("Got response: " + res.statusCode);
            }).on('error', function(e) {
                console.log("Got error: " + e.message);
            });
            
        } else if ((req.url).match(/^\/api\/[a-zA-Z0-9]+\/lights\/([a-zA-Z0-9]+)$/)) {
            var statePath = path.join(__dirname, config.get("bookmarks.states"));
            var state = JSON.parse(fs.readFileSync(statePath));
            var states = {};
            
            for (i in state.lights) {    
                if ((req.url).indexOf(i)>0) {
                    console.log((req.url).indexOf(i));
                    console.log("config/" + (req.url).substr((req.url).indexOf(i), i.length )+ ".json");
                    states = JSON.parse(fs.readFileSync("config/" + (req.url).substr((req.url).indexOf(i), i.length )+ ".json"));
                    res.writeHead(200, {
                        'Content-Type': 'application/json'
                    });


                    res.write(JSON.stringify(states));
                    res.end();
                    console.log(states);
                    break;
                }
                
                
                
            }
        } else if ((req.url).match(/^(\/api\/[a-zA-Z0-9]+)(\/groups\/[0-9])$/)) {
            console.log("query for groups -- not yet implemented");
        }

        res.end();
    }

});
server.listen(9000, ip.address());
/** ************************************************ **/


// some helper functions
function parseLines(lines) {
    var headers = {};
    for (var i = 1; i < lines.length; i++) {
        var colonPos = lines[i].indexOf(':');
        if (colonPos === -1) continue;
        headers[lines[i].substr(0, colonPos).toUpperCase()] = lines[i].substr(colonPos + 1).trim();
    }
    return headers;
}
function parseMsearchRequest(msg) {
    var lines = msg.toString().split('\r\n');
    if (!/^M-SEARCH \* HTTP\/1.1/.test(lines[0])) return false;
    return parseLines(lines);
}
 
function parseMsearchResponse(msg) {
    var lines = msg.toString().split('\r\n');
    if (!/^HTTP\/1.1 200 OK/.test(lines[0])) return null;
    return parseLines(lines);
}
